<!doctype html>
<html class="no-js" lang="br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tela Usuario</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main2.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <link rel="stylesheet" href="css/estilo4.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<style>
    .body{
        background-image: url("imagens/foto-decoracao-mecanica.png");
        background-repeat: no-repeat;
    }
</style>
<body class="body">
    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="nalika-profile">
                <h1 class="classe1 fadeIn2 ">SECURE CARFIX</h1>
                <div class="profile-dtl">
            <?php
            session_start();
            $host = "localhost";
            $user = "root";
            $senha = "";
            $db = "securecarfix";
            $mysqli = new mysqli($host, $user, $senha, $db);

            if(isset($_SESSION['user'])) {
                $nome = $_SESSION['user'];
            }
            $sql = "select id_cliente from cliente where usuario = \"".$nome."\" ";
            $idres = mysqli_query($mysqli, $sql);
            $idcli = mysqli_fetch_assoc($idres)['id_cliente'];
            $resu = "select img_user from cliente where id_cliente = '".$idcli."' ";
            $res = $mysqli->query($resu) or die ($mysqli->error);
            while ($img = $res->fetch_array()){?>
            <img src="<?php echo "Img_Usuarios/".$img["img_user"];?>" alt="Nulo" />
            <h2><?php
                echo 'Bem-Vindo, <br>'.ucfirst($nome);
                ?>
            </h2>
    </div>
    </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <li>
                            <a href="PUVeiculos.php">
                                <i class="icon icon-wrap"></i>
                                <span class="mini-click-non">Veículos</span>
                            </a>
                        </li>
                        <li>
                            <a href="PaginaCronograma.php">
                                <i class="icon icon-wrap"></i>
                                <span class="mini-click-non">Cronograma</span>
                            </a>
                        </li>
                        <li>
                            <a href="PaginaConfig.php">
                                <i class="icon icon-wrap"></i>
                                <span class="mini-click-non">Configurações</span>
                            </a>
                        </li>
                        <li>
                            <a href="logout.php?token='.md5(session_id()).'">
                                <i class="icon icon-wrap"></i>
                                <span class="mini-click-non">Sair</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">

                </div>
            </div>
            <!-- Mobile Menu start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">
                                        <li><a href="PUVeiculos.php">Veículos</a></li>
                                        <li><a href="PaginaCronograma.php">Cronograma</a></li>
                                        <li><a href="PaginaConfig.php">Configurações</a></li>
                                        <li><a href="logout.php?token='.md5(session_id()).'">Sair</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
<!--            -------------------------------------->
    <style>
        .profile-dt2 {
            text-align: center;
        }
        .profile-dt2 img {
            width: 200px;
            height: 200px;
            border-radius: 50%;
            border: 2px solid #152036;
        }
        .profile-dt2 h2{
            font-size:16px;
            color:#fff;
            font-weight:400;
            margin: 10px 0px 15px 0px;
        }
        .resp{
            width: 650px;
        }
        .resp2{
            margin-left: 220px;

        }
        .resp3{
            margin-left: 150px;margin-right: 150px;
        }
        .resp4{
            margin-left: 175px;
            margin-right: 175px;
        }
        .resp5{
            margin-left: 220px;
        }
        @media only screen and (max-width: 750px){
            .resp{
                width: auto;
            }
            .resp2{
                margin: auto;
            }
            .resp3{
                margin: auto;
            }
            .resp4{
                margin: auto;
            }
            .resp5{
                margin-left: 75px;
            }
        }
    </style>
    <div class="all-content-wrapper">
            <div class="modal-content resp" style="margin: 60px;">
                <div class="modal-header">
                    <h5 class="modal-title" style="text-align: center;font-size: 30px;font-family: Roboto;" id="exampleModalLabel">Configurações</h5>
                </div>
                <div class="modal-body">
                    <form action="edit_usu.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="recipient-name" style="font-size: 30px;" class="resp5 col-form-label">Foto De Perfil</label>
                            <br>
                            <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                            <div class="profile-dt2" style="height: 100px">
                                <img src="<?php echo "Img_Usuarios/".$img["img_user"];?>" alt="Nulo" />
                                <br>
                                <br>
                                <input class="resp2" name="img" type="file"/>

                            </div>

                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="form-group resp4" >
                            <label for="recipient-name" class="col-form-label">Usuario <a style="color: red">*</a></label>
                            <input type="text" placeholder="Usuario" class="form-control" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" id="recipient-name" name="usernew">
                        </div>
                        <div class="form-group resp3" >
                            <label for="recipient-name" class="col-form-label">Senha Antiga<a style="color: red">*</a></label>
                            <input type="password" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" name="senhaold" size="8" maxlength="8">
                        </div>
                        <div class="form-group resp3" >
                            <label for="recipient-name" class="col-form-label">Senha Nova <a style="color: red">*</a></label>
                            <input type="password" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" size="4" maxlength="4" name="senhanew" pattern="[0-9]+$" >
                        </div>
                        <div class="form-group resp3" >
                            <label for="recipient-name" class="col-form-label">Confirmar Senha Nova <a style="color: red">*</a></label>
                            <input type="password" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" size="4" maxlength="4" name="senhanewconfirm" pattern="[0-9]+$" >
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <input type="submit" class="btn btn-success btn-lg" value="Alterar">
                        </div>
                    </form>
                </div>
            </div>
<?php }?>





<!--            -------------------------------------->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
	<!-- float JS
		============================================ -->
    <script src="js/flot/jquery.flot.js"></script>
    <script src="js/flot/jquery.flot.resize.js"></script>
    <script src="js/flot/curvedLines.js"></script>
    <script src="js/flot/flot-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main2.js"></script>
</body>

</html>