<?php
session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Secure Car Fix</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="animacaoes/animate.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <link href="css/estilo3.css" rel="stylesheet">
    <link rel="stylesheet" href="css/css1.css" >
    <link rel="stylesheet" href="css/css2.css" >
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
        </style>
        <!-- Custom styles for this template -->
        <link href="carousel.css" rel="stylesheet">
    </head>
</head>
<body>
<div id="home"></div>
<nav id="menu"  style="height: 55px" class="navbar navbar-expand-md navbar-dark fixed-top ">
    <a class="navbar-brand scroll" style="font-family: Roboto;" href="#home">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="nav navbar-nav navbar-right ml-auto">
            <li class="nav-item rounded" style="padding-top: 20px">
                <a  style="font-family: Roboto;font-size: 20px" data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">Login</a>
                <label for="login" >
                <ul class="dropdown-menu form-wrapper ">
                    <li>
                        <form action="login.php" method="post">
                            <p class="hint-text">Faça o Login!</p>
                            <div class="form-group ">
                                <input type="text" class="form-control" placeholder="Usuario" required="required" name="usuario">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Senha" required="required" name="senha">
                            </div>
                            <input type="submit" class="btn btn-info btn-block" value="Login">
                        </form>
                        <p class="text-center text-danger">
                            <?php if(isset($_SESSION['loginErro'])){
                                echo $_SESSION['loginErro'];
                                unset($_SESSION['loginErro']);
                            }?>
                        </p>
                    </li>
                </ul></label>
            </li>
            <li class="nav-item" style="padding-top: 20px">
                <a href="#" data-toggle="dropdown" style="font-family: Roboto;font-size: 20px" class="nav-link dropdown-toggle">Cadastro</a>
                <ul class="dropdown-menu form-wrapper">
                    <li>
                        <form action="cadastro.php" method="post">
                            <p class="hint-text">Faça Seu Cadastro!</p>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Usuario" required="required" name="user">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email" required="required" name="email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="CPF" required="required" name="cpf">
                            </div>
                            <div class="form-group">
                                <p class="hint-text">Data de Nascimento</p>
                                <input type="date" class="form-control" placeholder="Data" required="required" name="data">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Senha" required="required" name="senha">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Confirmação da senha" required="required" name="csenha">
                            </div>
                            <input type="submit" class="btn btn-info btn-block" value="Cadastrar">
                        </form>
                            <?php if(isset($_SESSION['ok'])){
                            echo $_SESSION['ok'];
                            unset($_SESSION['ok']);} ;?>
                        <p class="text-center text-danger">
                            <?php if(isset($_SESSION['senhaerro'])){
                                echo $_SESSION['senhaerro'];
                                unset($_SESSION['senhaerro']);
                            }?>
                        </p>
                        <p class="text-center text-danger">
                            <?php if(isset($_SESSION['usuarioexistente'])){
                                echo $_SESSION['usuarioexistente'];
                                unset($_SESSION['usuarioexistente']);
                            }?>
                        </p>
                        <p class="text-center text-danger">
                            <?php if(isset($_SESSION['emailexistente'])){
                                echo $_SESSION['emailexistente'];
                                unset($_SESSION['emailexistente']);
                            }?>
                        </p>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Cadastrado com sucesso!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary align-middle btn-info" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<header style="height: 825px">
    <video data-speed="6" data-type="background" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" height="825px">
        <source src="videos/video_capa.webm">
    </video>
    <div class="container" >
        <h1 class="classe1 fadeIn2 "><span style="font-family: bignoodle;">SECURE CAR FIX</span></h1>

        <div class="text-md-center">
         <a style="font-size: 35px;color: white;font-family: CaviarDreams;" class="fadeIn2 text-capitalize align-middle">Quem Somos?</a>
        </div>
    </div>
</header>
<br>
<br>
<br>
<main role="main">
    <div class="container marketing">


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Veículos e a Indústria Automobilística</h2><br>
                <p class="lead">Os veículos e a indústria automobilística são uma das coisas mais importantes no mundo. Com mais de 1.2 bilhões de carros no mundo, é notável que muitas pessoas tiveram suas vidas mudadas devido aos veículos. Com isso, a indústria automobilística está em constante evolução, contando com modernidade em seus designs, peças, funcionalidades, e muitas outras atualizações que, em muitos casos, revolucionaram os veículos.</p>
            </div>
            <div class="col-md-5">
               <img name="img" src="imagens/foto-1-inicial.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-8 order-md-2">
                <h2 class="featurette-heading">Quem manda é <span class="text-muted">Você!</span></h2><br>
                <p class="lead">Apesar da gigantesca quantidade de veículos e da alta necessidade dos mesmos, os veículos podem apresentar vários tipos de problemas, mesmo com a evolução das peças e mecânicas. Para o auxílio de donos de veículos, o desenvolvimento desse protótipo irá ajudar a tornar a revisão mais tecnológica, fazendo com que seja muito mais fácil acompanhar a revisão de um veículo, também informando melhor o dono do veículo sobre o que foi revisado, trocado ou atualizado.</p>
            </div>
            <div class="col-md-4 order-md-1">
                <img name="img" src="imagens/foto-2-inicial.png">
            </div>
        </div>

        <hr class="featurette-divider">

    </div>

</main>
<!----------------><!---------------->
<footer class="footer">

    <div class="container">
        <ul class="foote_bottom_ul_amrc">
            <li class="">
                <a class=" scroll" style="font-family: CaviarDreams;" href="#home">Home</a>
            </li>
        </ul>
    </div>
    <!------------------------------------------->
    <p class="text-center">Copyright @2019 | Criado Por <a href="https://www.facebook.com/Lannk09">Luan Carlos Xavier</a> e <a href="https://www.facebook.com/leooo172">Leonardo Vieira</a></p>

    <!------------------------------------------->

</footer>
<script src="js/js1.js" ></script>
<script src="js/js2.js" ></script>
<script src="js/js3.js" ></script>	<script type="text/javascript">
</script>
<script src="js/js4.js"></script>
<script src="js/js5.js"></script>
<script type="text/javascript">
    jQuery(function () {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() <= 150) {
                $("#menu").addClass("bg-transparent").removeClass("bg-dark animated fadeIn ");
            } else {
                $("#menu").removeClass("bg-transparent").addClass("bg-dark animated fadeIn shadow-sm ");
            }
        });
    });
</script>

<script>
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
        });
    });
</script>
</body>
</html>