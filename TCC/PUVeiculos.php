<!doctype html>
<html class="no-js" lang="br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Veículos</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main2.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/estilo4.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<style>
    .card
    {
        color: white;
        position:relative;
        display:-ms-flexbox;
        display:flex;-ms-flex-direction:column;
        flex-direction:column;min-width:0;
        word-wrap:break-word;
        background-color: #213357;
        background-clip:border-box;
        border:1px solid rgba(109, 154, 198, 0.8);
        border-radius:.25rem
    }

    .card-img-top{border-top-right-radius:0}
    .card-body{-ms-flex:1 1 auto;flex:1 1 auto;padding:1.25rem}
    .card-title{margin-bottom:.75rem}
    .card-text:last-child{margin-bottom:0}
    .body{
        background-image: url("imagens/foto-decoracao-mecanica.png");
        background-repeat: no-repeat;
    }
</style>
<body class="body">
    <div class="left-sidebar-pro" >
        <nav id="sidebar" class="">

			<div class="nalika-profile">
                <h1 class="classe1 fadeIn2 ">SECURE CARFIX</h1>
                <div class="profile-dtl">
            <?php
            session_start();
            $host = "localhost";
            $user = "root";
            $senha = "";
            $db = "securecarfix";
            $mysqli = new mysqli($host, $user, $senha, $db);

            if(isset($_SESSION['user'])) {
                $nome = $_SESSION['user'];
            }
            $sql = "select id_cliente from cliente where usuario = \"".$nome."\" ";
            $idres = mysqli_query($mysqli, $sql);
            $idcli = mysqli_fetch_assoc($idres)['id_cliente'];
            $resu = "select img_user from cliente where id_cliente = '".$idcli."' ";
            $res = $mysqli->query($resu) or die ($mysqli->error);
            while ($img = $res->fetch_array()){?>
            <img src="<?php echo "Img_Usuarios/".$img["img_user"];?>" alt="Nulo" />
					<h2><?php
                            echo 'Bem-Vindo, <br>'.ucfirst($nome);
                        }?>
                    </h2>
			</div>
			</div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <li class="active">
                        </li>
                        <li>
                            <a href="PUVeiculos.php">
                                <i class="icon icon-wrap"></i>
                                <span class="mini-click-non">Veículos</span>
                            </a>
                        </li>
                        <li>
                            <a href="PaginaCronograma.php">
                                <i class="icon icon-wrap"></i>
                                <span class="mini-click-non">Cronograma</span>
                            </a>
                        </li>
                        <li>
                            <a href="PaginaConfig.php">
                                <i class="icon icon-wrap"></i>
                                <span class="mini-click-non">Configurações</span>
                            </a>
                        </li>
                        <li>
                            <a href="logout.php?token='.md5(session_id()).'">
                                <i class="icon icon-wrap"></i>
                                <span class="mini-click-non">Sair</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
    <!-- Start Welcome area -->

    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">

                </div>
            </div>
            <!-- Mobile Menu start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">

                                                <li><a href="PUVeiculos.php">Veículos</a></li>
                                                <li><a href="PaginaCronograma.php">Cronograma</a></li>
                                                <li><a href="PaginaConfig.php">Configurações</a></li>
                                                <li><a href="logout.php?token='.md5(session_id()).'">Sair</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu end -->
            <button type="button" class="btn btn-primary btn-lg btn-info" style="margin: 30px;margin-top: 40px;font-size: 35px;font-family:bignoodle;background-color: #1b2a47;" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><img src="imagens/Mais.png" style="height: 60px;width: auto;"><br>Cadastrar Novo Veículo</button>
<!--MODAL-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" style="text-align: center;font-size: 30px;font-family: Roboto;" id="exampleModalLabel">Cadastrar Veículos</h5>
                        </div>
                        <div class="modal-body">
                            <form action="cadastroCarro.php" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Modelo do Veículo <a style="color: red">*</a></label>
                                    <input type="text" class="form-control" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" id="recipient-name" name="modelo">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Placa do Veículo <a style="color: red">*</a></label>
                                    <input type="text" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" name="placa" size="8" maxlength="8">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Ano do Veículo <a style="color: red">*</a></label>
                                    <input type="text" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" size="4" maxlength="4" name="ano" pattern="[0-9]+$" >
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Características do Veículo <a style="color: red">*</a></label>
                                    <textarea style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" rows="3" name="carac"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Imagem Referente ao Veículo</label>
                                    <br>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                                    <div><input name="img" type="file"/></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                    <input type="submit" class="btn btn-primary btn-lg" value="Cadastrar">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        <div class="container-fluid">
            <div class="row">
             <?php

             $sql = "select id_cliente from cliente where usuario = \"".$nome."\" ";
             $idres = mysqli_query($mysqli, $sql);
             $idcli = mysqli_fetch_assoc($idres)['id_cliente'];
             $resu = "select * from carro where id_cliente = '".$idcli."' ";
             $res = $mysqli->query($resu) or die ($mysqli->error);
             while ($dado = $res->fetch_array()){
             ?>

                    <div class="col-sm-3">
                        <div class="card " style="width: 300px; margin: 15px;">
                            <!--imagem
                            ==============================================-->
                            <img class="card-img-top" src="<?php echo "Img_Uploads/".$dado["imgcar"];?>" alt="Não Possui Imagem" style="height: 200px;width: 300px">

                            <div class="card-body">
                                <!--Nome Do Dono
                                ==================================================-->
                                <!--Modelo / Placa
                          ==================================================-->
                                <h4 class="card-title"><?php echo $dado["modelo"]; ?> - <?php echo $dado["placa"]; ?></h4>
                                <!--caracteristicas
                                ==============================================-->
                                <p class="card-text"><?php echo $dado["carac"]; ?></p>
                                <!--Barra de status
                                ============================================-->
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalExemplo1<?php echo $dado["id_carro"]; ?>" data-whatever="<?php echo $dado["id_carro"]; ?>" >Status</button>
                            </div>
                        </div>
                    </div>
        <div class="modal fade" id="modalExemplo1<?php echo $dado["id_carro"]; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" style="color: black;font-size: 30px" id="exampleModalLabel">Status</h5>
                    </div>
                    <div class="modal-body" style="color: black">
                            <p class="card-text"><?php
                                if ($dado["status"]==1){
                                    echo "<a class='text-danger' style='font-size: 25px'>Solicitando conserto<a/><br><br>";
                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >".utf8_encode($dado["obs"])."</textarea>";
                                }
                                if ($dado["status"]==2){
                                    echo "<a class='text-warning' style='font-size: 25px'>Em Andamento<a/><br><br>";
                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >".utf8_encode($dado["obs"])."</textarea>";
                                }
                                if ($dado["status"]==3) {
                                    echo "<a class='text-success' style='font-size: 25px'>Pronto Para Retirada!<a/><br><br>";
                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >".utf8_encode($dado["obs"])."</textarea>";
                                    ?><br><br>
                                    <video width="400" controls="controls" >
                                        <source  type="video/mp4" src="<?php echo "Video_Up/".$dado["video"];?>">
                                    </video>
                                <?php } ?>
                            </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
            <?php } ?>
        </div>
    </div>

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
	<!-- float JS
		============================================ -->
    <script src="js/flot/jquery.flot.js"></script>
    <script src="js/flot/jquery.flot.resize.js"></script>
    <script src="js/flot/curvedLines.js"></script>
    <script src="js/flot/flot-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main2.js"></script>
    <script type="text/javascript">
        $('#modalExemplo1').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            var mod = button.data('whatevermod') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('#id').val(recipient)
        })
    </script>
</body>

</html>