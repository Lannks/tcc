<?php
session_start();
include_once("conexao.php");
$result_events = "SELECT id, title, color, start, end FROM events";
$resultado_events = mysqli_query($conexao, $result_events);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Veículos</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- nalika Icon CSS
        ============================================ -->
    <link rel="stylesheet" href="css/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main2.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/estilo4.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <meta charset='utf-8' />
    <link href='css/fullcalendar.min.css' rel='stylesheet' />
    <link href='css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src='js/moment.min.js'></script>
    <script src='js/jquery.min.js'></script>
    <script src='js/fullcalendar.min.js'></script>
    <script src='locale/pt-br.js'></script>
    <script>
        $(document).ready(function() {
            $('#calendar1').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month'
                },
                defaultDate: Date(),
                navLinks: true,
                editable: true,
                eventLimit: true,
                events: [
                    <?php
                    while($row_events = mysqli_fetch_array($resultado_events)){
                    ?>
                    {
                        id: '<?php echo $row_events['id']; ?>',
                        title: '<?php echo $row_events['title']; ?>',
                        start: '<?php echo $row_events['start']; ?>',
                        end: '<?php echo $row_events['end']; ?>',
                        color: '<?php echo $row_events['color']; ?>',
                    },<?php
                    }
                    ?>
                ]
            });
        });
    </script>
</head>
<style>
    .body{
        background-image: url("imagens/foto-decoracao-mecanica.png");
        background-repeat: no-repeat;
    }
</style>
<body class="body">
<div class="left-sidebar-pro" >
    <nav id="sidebar" class="">

        <div class="nalika-profile">
            <h1 class="classe1 fadeIn2 ">SECURE CARFIX</h1>
            <div class="profile-dtl">
                <?php
                $host = "localhost";
                $user = "root";
                $senha = "";
                $db = "securecarfix";
                $mysqli = new mysqli($host, $user, $senha, $db);

                if(isset($_SESSION['user'])) {
                    $nome = $_SESSION['user'];
                }
                $sql = "select id_cliente from cliente where usuario = \"".$nome."\" ";
                $idres = mysqli_query($mysqli, $sql);
                $idcli = mysqli_fetch_assoc($idres)['id_cliente'];
                $resu = "select img_user from cliente where id_cliente = '".$idcli."' ";
                $res = $mysqli->query($resu) or die ($mysqli->error);
                while ($img = $res->fetch_array()){?>
                <img src="<?php echo "Img_Usuarios/".$img["img_user"];?>" alt="Nulo" />
                <h2><?php
                    echo 'Bem-Vindo, <br>'.ucfirst($nome);
                    }?>
                </h2>
            </div>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li>
                        <a href="PUVeiculos.php">
                            <i class="icon icon-wrap"></i>
                            <span class="mini-click-non">Veículos</span>
                        </a>
                    </li>
                    <li>
                        <a href="PaginaCronograma.php">
                            <i class="icon icon-wrap"></i>
                            <span class="mini-click-non">Cronograma</span>
                        </a>
                    </li>
                    <li>
                        <a href="PaginaConfig.php">
                            <i class="icon icon-wrap"></i>
                            <span class="mini-click-non">Configurações</span>
                        </a>
                    </li>
                    <li>
                        <a href="logout.php?token='.md5(session_id()).'">
                            <i class="icon icon-wrap"></i>
                            <span class="mini-click-non">Sair</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </nav>
</div>
<!-- Mobile Menu start -->
<div class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul class="mobile-menu-nav">
                            <li><a href="PUVeiculos.php">Veículos</a></li>
                            <li><a href="PaginaCronograma.php">Cronograma</a></li>
                            <li><a href="PaginaConfig.php">Configurações</a></li>
                            <li><a href="logout.php?token='.md5(session_id()).'">Sair</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="all-content-wrapper">
    <div class="container-fluid">
<!--====================-->
        <style>
            .back{
                width: 650px;
                height: 650px;
            }
            @media only screen and (max-width: 750px){
                .back{
                    width: auto;
                }
            }
        </style>

<div id='calendar1' class="back" style="margin-top: 40px"></div>
<!--====================-->
</body>
<script src="js/bootstrap.min.js"></script>
<!-- wow JS
    ============================================ -->
<script src="js/wow.min.js"></script>
<!-- price-slider JS
    ============================================ -->
<script src="js/jquery-price-slider.js"></script>
<!-- meanmenu JS
    ============================================ -->
<script src="js/jquery.meanmenu.js"></script>
<!-- owl.carousel JS
    ============================================ -->
<script src="js/owl.carousel.min.js"></script>
<!-- sticky JS
    ============================================ -->
<script src="js/jquery.sticky.js"></script>
<!-- scrollUp JS
    ============================================ -->
<script src="js/jquery.scrollUp.min.js"></script>
<!-- mCustomScrollbar JS
    ============================================ -->
<script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/scrollbar/mCustomScrollbar-active.js"></script>
<!-- metisMenu JS
    ============================================ -->
<script src="js/metisMenu/metisMenu.min.js"></script>
<script src="js/metisMenu/metisMenu-active.js"></script>
<!-- sparkline JS
    ============================================ -->
<script src="js/sparkline/jquery.sparkline.min.js"></script>
<script src="js/sparkline/jquery.charts-sparkline.js"></script>
<!-- float JS
    ============================================ -->
<script src="js/flot/jquery.flot.js"></script>
<script src="js/flot/jquery.flot.resize.js"></script>
<script src="js/flot/curvedLines.js"></script>
<script src="js/flot/flot-active.js"></script>
<!-- plugins JS
    ============================================ -->
<script src="js/plugins.js"></script>
<!-- main JS
    ============================================ -->
<script src="js/main2.js"></script>
</html>
