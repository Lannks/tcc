<?php
include('conexao.php');
include('PAdmVeiculos.php');

if (isset($_POST['lista'])){ $listar = $_POST['lista'];
}
//--------------------------
?>    <div class="all-content-wrapper"> <div class="container-fluid"><?php
if ($listar==1) { ?><div class="row"><?php
    $result_usuario = "SELECT * FROM carro WHERE status='$listar'";
    $resultado_usuario = mysqli_query($mysqli, $result_usuario);
    $row = mysqli_num_rows($resultado_usuario);
    if ($row > 0) {
        while ($dado = mysqli_fetch_array($resultado_usuario)) {
            ?>

                        <div class="col-sm-3">
                            <div class="card " style="width: 300px; margin: 15px;">
                                <!--imagem
                                ==============================================-->
                                <img class="card-img-top" src="<?php echo "Img_Uploads/" . $dado["imgcar"]; ?>"
                                     alt="Não Possui Imagem" style="height: 200px;width: 300px">

                                <div class="card-body">
                                    <!--Nome Do Dono
                                    ==================================================-->
                                    <!--Modelo / Placa
                              ==================================================-->
                                    <h4 class="card-title"><?php echo $dado["modelo"]; ?>
                                        - <?php echo $dado["placa"]; ?></h4>
                                    <!--caracteristicas
                                    ==============================================-->
                                    <p class="card-text"><?php echo $dado["carac"]; ?></p>
                                    <!--Barra de status
                                    ============================================-->
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalExemplo1<?php echo $dado["id_carro"]; ?>" data-whatever="<?php echo $dado["id_carro"]; ?>">Status</button>
                                    <!--Barra de Editar
                                    ===========================================-->
                                    <button type="button" style="margin-left: 23px" class="btn btn-primary"
                                            data-toggle="modal" data-target="#modalExemplo2"
                                            data-whatever="<?php echo $dado["id_carro"]; ?>">Editar
                                    </button>
                                    <!--Barra de Exclusao
                                    =======================================================================-->
                                    <button type="button" style="margin-left: 23px" class="btn btn-danger"
                                            data-toggle="modal" data-target="#modalExemplo3"
                                            data-whatever="<?php echo $dado["id_carro"]; ?>">Excluir
                                    </button>
                                    <!--======================================================================-->
                                </div>
                            </div>
                        </div>
                        <!-------------------------------------------->
                        <div class="modal fade" id="modalExemplo2" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content" style="color: black">
                                    <form action="edit_carros.php" enctype="multipart/form-data" method="post">
                                        <div class="form-group">
                                            <input type="hidden" contenteditable="false" class="form-control" id="id"
                                                   name="id">
                                        </div>
                                        <div class="modal-header">
                                            <h5 class="modal-title"
                                                style="text-align: center;font-size: 30px;font-family: Roboto;"
                                                id="exampleModalLabe">Editar Veículo</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="mod" class="col-form-label">Modelo do Veículo <a
                                                        style="color: red">*</a></label>
                                                <input type="text"
                                                       style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                       class="form-control" id="recipient-name" name="modelo">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Placa do Veículo <a
                                                        style="color: red">*</a></label>
                                                <input type="text"
                                                       style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                       class="form-control" id="recipient-name" name="placa" size="8"
                                                       maxlength="8">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Ano do Veículo <a
                                                        style="color: red">*</a></label>
                                                <input type="text"
                                                       style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                       class="form-control" id="recipient-name" size="4" maxlength="4"
                                                       name="ano" pattern="[0-9]+$">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Características do
                                                    Veículo <a style="color: red">*</a></label>
                                                <textarea
                                                    style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                    class="form-control" id="recipient-name" rows="3"
                                                    name="carac"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label ">Imagem Referente ao
                                                    Veículo</label>
                                                <br>
                                                <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                                                <div><input name="img" type="file"/></div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Fechar
                                            </button>
                                            <input type="submit" class="btn btn-primary btn-lg" value="Editar">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--------------------------->
                        <div class="modal fade" id="modalExemplo3" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="excluir.php" method="post">
                                        <div class="form-group">
                                            <input type="hidden" contenteditable="false" class="form-control" id="id"
                                                   name="id">
                                        </div>
                                        <div class="modal-header">
                                            <h5 class="modal-title" style="color: black;font-size: 30px"
                                                id="exampleModalLabel">Deseja Realmente Excluir?</h5>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Fechar
                                            </button>
                                            <input type="submit" class="btn btn-danger btn-lg" value="Excluir">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--------------------------->
                        <div class="modal fade" id="modalExemplo1<?php echo $dado["id_carro"]; ?>" tabindex="-1"
                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="color: black;font-size: 30px"
                                            id="exampleModalLabel">Status</h5>
                                    </div>
                                    <div class="modal-body" style="color: black">
                                        <form method="post" action="edit_status.php" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <input type="hidden" value="<?php echo $dado["id_carro"]; ?>"
                                                       contenteditable="false" class="form-control" name="id">
                                            </div>
                                            <label class="text-primary"
                                                   style="text-align: center;font-size: 20px;font-family: Roboto;">Status
                                                desejado :</label><br>
                                            <select name="status">
                                                <option name="status" value="0">Selecione o Status</option>
                                                <option class="text-danger" name="status" value="1">Solicitando
                                                    Conserto
                                                </option>
                                                <option class="text-warning" name="status" value="2">Em Andamento
                                                </option>
                                                <option class="text-success" name="status" value="3">Pronto Para
                                                    Retirada
                                                </option>
                                            </select>
                                            <br>
                                            <br>
                                            <label class="text-primary"
                                                   style="text-align: center;font-size: 20px;font-family: Roboto;">Observações:</label>
                                            <textarea
                                                style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;"
                                                class="form-control" id="recipient-name"
                                                name="obs"><?php echo utf8_encode($dado["obs"]); ?></textarea>
                                            <br>
                                            <label for="recipient-name" class="col-form-label text-primary">Video de
                                                Alterações</label>
                                            <br>
                                            <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                                            <div><input name="video" type="file"/></div>
                                            <br>
                                            <label class="text-big"
                                                   style="text-align: center;font-size: 20px;font-family: Roboto;">Status
                                                Atual :</label><br>
                                            <label style="text-align: center;font-size: 20px;font-family: Roboto;">-----------------------------------------------------</label><br>

                                            <p class="card-text"><?php
                                                if ($dado["status"] == 1) {
                                                    echo "<a class='text-danger' style='font-size: 25px'>Solicitando conserto<a/><br><br>";
                                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                                }
                                                if ($dado["status"] == 2) {
                                                    echo "<a class='text-warning' style='font-size: 25px'>Em Andamento<a/><br><br>";
                                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                                }
                                                if ($dado["status"] == 3) {
                                                    echo "<a class='text-success' style='font-size: 25px'>Pronto Para Retirada!<a/><br><br>";
                                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                                    ?><br><br>
                                                    <video width="400" controls="controls">
                                                        <source type="video/mp4"
                                                                src="<?php echo "Video_Up/" . $dado["video"]; ?>">
                                                    </video>
                                                <?php } ?>
                                            </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar
                                        </button>
                                        <input type="submit" class="btn btn-primary btn-lg" value="Editar">
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

   <?php } ?>
            </div>
        <?php
    }
}
elseif ($listar==2) { ?><div class="row"><?php
    $result_usuario = "SELECT * FROM carro WHERE status='$listar'";
    $resultado_usuario = mysqli_query($mysqli, $result_usuario);
    $row = mysqli_num_rows($resultado_usuario);
    if ($row > 0) {
        while ($dado = mysqli_fetch_array($resultado_usuario)) {
            ?>
                    <div class="col-sm-3">
                        <div class="card " style="width: 300px; margin: 15px;">
                            <!--imagem
                            ==============================================-->
                            <img class="card-img-top" src="<?php echo "Img_Uploads/" . $dado["imgcar"]; ?>"
                                 alt="Não Possui Imagem" style="height: 200px;width: 300px">

                            <div class="card-body">
                                <!--Nome Do Dono
                                ==================================================-->
                                <!--Modelo / Placa
                          ==================================================-->
                                <h4 class="card-title"><?php echo $dado["modelo"]; ?>
                                    - <?php echo $dado["placa"]; ?></h4>
                                <!--caracteristicas
                                ==============================================-->
                                <p class="card-text"><?php echo $dado["carac"]; ?></p>
                                <!--Barra de status
                                ============================================-->
                                <button type="button" class="btn btn-success" data-toggle="modal"
                                        data-target="#modalExemplo1<?php echo $dado["id_carro"]; ?>"
                                        data-whatever="<?php echo $dado["id_carro"]; ?>">Status
                                </button>
                                <!--Barra de Editar
                                ===========================================-->
                                <button type="button" style="margin-left: 23px" class="btn btn-primary"
                                        data-toggle="modal" data-target="#modalExemplo2"
                                        data-whatever="<?php echo $dado["id_carro"]; ?>">Editar
                                </button>
                                <!--Barra de Exclusao
                                =======================================================================-->
                                <button type="button" style="margin-left: 23px" class="btn btn-danger"
                                        data-toggle="modal" data-target="#modalExemplo3"
                                        data-whatever="<?php echo $dado["id_carro"]; ?>">Excluir
                                </button>
                                <!--======================================================================-->
                            </div>
                        </div>
                    </div>
                    <!-------------------------------------------->
                    <div class="modal fade" id="modalExemplo2" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content" style="color: black">
                                <form action="edit_carros.php" enctype="multipart/form-data" method="post">
                                    <div class="form-group">
                                        <input type="hidden" contenteditable="false" class="form-control" id="id"
                                               name="id">
                                    </div>
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            style="text-align: center;font-size: 30px;font-family: Roboto;"
                                            id="exampleModalLabe">Editar Veículo</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="mod" class="col-form-label">Modelo do Veículo <a
                                                    style="color: red">*</a></label>
                                            <input type="text"
                                                   style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                   class="form-control" id="recipient-name" name="modelo">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Placa do Veículo <a
                                                    style="color: red">*</a></label>
                                            <input type="text"
                                                   style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                   class="form-control" id="recipient-name" name="placa" size="8"
                                                   maxlength="8">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Ano do Veículo <a
                                                    style="color: red">*</a></label>
                                            <input type="text"
                                                   style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                   class="form-control" id="recipient-name" size="4" maxlength="4"
                                                   name="ano" pattern="[0-9]+$">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Características do
                                                Veículo <a style="color: red">*</a></label>
                                            <textarea
                                                style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                class="form-control" id="recipient-name" rows="3"
                                                name="carac"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label ">Imagem Referente ao
                                                Veículo</label>
                                            <br>
                                            <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                                            <div><input name="img" type="file"/></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Fechar
                                        </button>
                                        <input type="submit" class="btn btn-primary btn-lg" value="Editar">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--------------------------->
                    <div class="modal fade" id="modalExemplo3" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="excluir.php" method="post">
                                    <div class="form-group">
                                        <input type="hidden" contenteditable="false" class="form-control" id="id"
                                               name="id">
                                    </div>
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="color: black;font-size: 30px"
                                            id="exampleModalLabel">Deseja Realmente Excluir?</h5>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Fechar
                                        </button>
                                        <input type="submit" class="btn btn-danger btn-lg" value="Excluir">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--------------------------->
                    <div class="modal fade" id="modalExemplo1<?php echo $dado["id_carro"]; ?>" tabindex="-1"
                         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" style="color: black;font-size: 30px"
                                        id="exampleModalLabel">Status</h5>
                                </div>
                                <div class="modal-body" style="color: black">
                                    <form method="post" action="edit_status.php" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <input type="hidden" value="<?php echo $dado["id_carro"]; ?>"
                                                   contenteditable="false" class="form-control" name="id">
                                        </div>
                                        <label class="text-primary"
                                               style="text-align: center;font-size: 20px;font-family: Roboto;">Status
                                            desejado :</label><br>
                                        <select name="status">
                                            <option name="status" value="0">Selecione o Status</option>
                                            <option class="text-danger" name="status" value="1">Solicitando
                                                Conserto
                                            </option>
                                            <option class="text-warning" name="status" value="2">Em Andamento
                                            </option>
                                            <option class="text-success" name="status" value="3">Pronto Para
                                                Retirada
                                            </option>
                                        </select>
                                        <br>
                                        <br>
                                        <label class="text-primary"
                                               style="text-align: center;font-size: 20px;font-family: Roboto;">Observações:</label>
                                        <textarea
                                            style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;"
                                            class="form-control" id="recipient-name"
                                            name="obs"><?php echo utf8_encode($dado["obs"]); ?></textarea>
                                        <br>
                                        <label for="recipient-name" class="col-form-label text-primary">Video de
                                            Alterações</label>
                                        <br>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                                        <div><input name="video" type="file"/></div>
                                        <br>
                                        <label class="text-big"
                                               style="text-align: center;font-size: 20px;font-family: Roboto;">Status
                                            Atual :</label><br>
                                        <label style="text-align: center;font-size: 20px;font-family: Roboto;">-----------------------------------------------------</label><br>

                                        <p class="card-text"><?php
                                            if ($dado["status"] == 1) {
                                                echo "<a class='text-danger' style='font-size: 25px'>Solicitando conserto<a/><br><br>";
                                                echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                            }
                                            if ($dado["status"] == 2) {
                                                echo "<a class='text-warning' style='font-size: 25px'>Em Andamento<a/><br><br>";
                                                echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                            }
                                            if ($dado["status"] == 3) {
                                                echo "<a class='text-success' style='font-size: 25px'>Pronto Para Retirada!<a/><br><br>";
                                                echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                                ?><br><br>
                                                <video width="400" controls="controls">
                                                    <source type="video/mp4"
                                                            src="<?php echo "Video_Up/" . $dado["video"]; ?>">
                                                </video>
                                            <?php } ?>
                                        </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar
                                    </button>
                                    <input type="submit" class="btn btn-primary btn-lg" value="Editar">
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
               <?php } ?>
            </div>
        <?php
    }
}
elseif ($listar==3) {
    ?><div class="row"><?php
    $result_usuario = "SELECT * FROM carro WHERE status='$listar'";
    $resultado_usuario = mysqli_query($mysqli, $result_usuario);
    $row = mysqli_num_rows($resultado_usuario);
    if ($row > 0) {
        while ($dado = mysqli_fetch_array($resultado_usuario)) {
            ?>
                    <div class="col-sm-3">
                        <div class="card " style="width: 300px; margin: 15px;">
                            <!--imagem
                            ==============================================-->
                            <img class="card-img-top" src="<?php echo "Img_Uploads/" . $dado["imgcar"]; ?>"
                                 alt="Não Possui Imagem" style="height: 200px;width: 300px">

                            <div class="card-body">
                                <!--Nome Do Dono
                                ==================================================-->
                                <!--Modelo / Placa
                          ==================================================-->
                                <h4 class="card-title"><?php echo $dado["modelo"]; ?>
                                    - <?php echo $dado["placa"]; ?></h4>
                                <!--caracteristicas
                                ==============================================-->
                                <p class="card-text"><?php echo $dado["carac"]; ?></p>
                                <!--Barra de status
                                ============================================-->
                                <button type="button" class="btn btn-success" data-toggle="modal"
                                        data-target="#modalExemplo1<?php echo $dado["id_carro"]; ?>"
                                        data-whatever="<?php echo $dado["id_carro"]; ?>">Status
                                </button>
                                <!--Barra de Editar
                                ===========================================-->
                                <button type="button" style="margin-left: 23px" class="btn btn-primary"
                                        data-toggle="modal" data-target="#modalExemplo2"
                                        data-whatever="<?php echo $dado["id_carro"]; ?>">Editar
                                </button>
                                <!--Barra de Exclusao
                                =======================================================================-->
                                <button type="button" style="margin-left: 23px" class="btn btn-danger"
                                        data-toggle="modal" data-target="#modalExemplo3"
                                        data-whatever="<?php echo $dado["id_carro"]; ?>">Excluir
                                </button>
                                <!--======================================================================-->
                            </div>
                        </div>
                    </div>
                    <!-------------------------------------------->
                    <div class="modal fade" id="modalExemplo2" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content" style="color: black">
                                <form action="edit_carros.php" enctype="multipart/form-data" method="post">
                                    <div class="form-group">
                                        <input type="hidden" contenteditable="false" class="form-control" id="id"
                                               name="id">
                                    </div>
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            style="text-align: center;font-size: 30px;font-family: Roboto;"
                                            id="exampleModalLabe">Editar Veículo</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="mod" class="col-form-label">Modelo do Veículo <a
                                                    style="color: red">*</a></label>
                                            <input type="text"
                                                   style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                   class="form-control" id="recipient-name" name="modelo">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Placa do Veículo <a
                                                    style="color: red">*</a></label>
                                            <input type="text"
                                                   style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                   class="form-control" id="recipient-name" name="placa" size="8"
                                                   maxlength="8">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Ano do Veículo <a
                                                    style="color: red">*</a></label>
                                            <input type="text"
                                                   style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                   class="form-control" id="recipient-name" size="4" maxlength="4"
                                                   name="ano" pattern="[0-9]+$">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Características do
                                                Veículo <a style="color: red">*</a></label>
                                            <textarea
                                                style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7"
                                                class="form-control" id="recipient-name" rows="3"
                                                name="carac"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label ">Imagem Referente ao
                                                Veículo</label>
                                            <br>
                                            <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                                            <div><input name="img" type="file"/></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Fechar
                                        </button>
                                        <input type="submit" class="btn btn-primary btn-lg" value="Editar">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--------------------------->
                    <div class="modal fade" id="modalExemplo3" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="excluir.php" method="post">
                                    <div class="form-group">
                                        <input type="hidden" contenteditable="false" class="form-control" id="id"
                                               name="id">
                                    </div>
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="color: black;font-size: 30px"
                                            id="exampleModalLabel">Deseja Realmente Excluir?</h5>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Fechar
                                        </button>
                                        <input type="submit" class="btn btn-danger btn-lg" value="Excluir">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--------------------------->
                    <div class="modal fade" id="modalExemplo1<?php echo $dado["id_carro"]; ?>" tabindex="-1"
                         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" style="color: black;font-size: 30px"
                                        id="exampleModalLabel">Status</h5>
                                </div>
                                <div class="modal-body" style="color: black">
                                    <form method="post" action="edit_status.php" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <input type="hidden" value="<?php echo $dado["id_carro"]; ?>"
                                                   contenteditable="false" class="form-control" name="id">
                                        </div>
                                        <label class="text-primary"
                                               style="text-align: center;font-size: 20px;font-family: Roboto;">Status
                                            desejado :</label><br>
                                        <select name="status">
                                            <option name="status" value="0">Selecione o Status</option>
                                            <option class="text-danger" name="status" value="1">Solicitando
                                                Conserto
                                            </option>
                                            <option class="text-warning" name="status" value="2">Em Andamento
                                            </option>
                                            <option class="text-success" name="status" value="3">Pronto Para
                                                Retirada
                                            </option>
                                        </select>
                                        <br>
                                        <br>
                                        <label class="text-primary"
                                               style="text-align: center;font-size: 20px;font-family: Roboto;">Observações:</label>
                                        <textarea
                                            style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;"
                                            class="form-control" id="recipient-name"
                                            name="obs"><?php echo utf8_encode($dado["obs"]); ?></textarea>
                                        <br>
                                        <label for="recipient-name" class="col-form-label text-primary">Video de
                                            Alterações</label>
                                        <br>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                                        <div><input name="video" type="file"/></div>
                                        <br>
                                        <label class="text-big"
                                               style="text-align: center;font-size: 20px;font-family: Roboto;">Status
                                            Atual :</label><br>
                                        <label style="text-align: center;font-size: 20px;font-family: Roboto;">-----------------------------------------------------</label><br>

                                        <p class="card-text"><?php
                                            if ($dado["status"] == 1) {
                                                echo "<a class='text-danger' style='font-size: 25px'>Solicitando conserto<a/><br><br>";
                                                echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                            }
                                            if ($dado["status"] == 2) {
                                                echo "<a class='text-warning' style='font-size: 25px'>Em Andamento<a/><br><br>";
                                                echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                            }
                                            if ($dado["status"] == 3) {
                                                echo "<a class='text-success' style='font-size: 25px'>Pronto Para Retirada!<a/><br><br>";
                                                echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >" . utf8_encode($dado["obs"]) . "</textarea>";
                                                ?><br><br>
                                                <video width="400" controls="controls">
                                                    <source type="video/mp4"
                                                            src="<?php echo "Video_Up/" . $dado["video"]; ?>">
                                                </video>
                                            <?php } ?>
                                        </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar
                                    </button>
                                    <input type="submit" class="btn btn-primary btn-lg" value="Editar">
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php
    }
}else{
    ?>
  <div class="row">
            <?php
            $sql = "select id_cliente from cliente where usuario = \"".$nome."\" ";
            $idres = mysqli_query($mysqli, $sql);
            $idcli = mysqli_fetch_assoc($idres)['id_cliente'];
            $resu = "select * from carro";
            $res = $mysqli->query($resu) or die ($mysqli->error);
            while ($dado = $res->fetch_array()){
            ?>
                    <div class="col-sm-3">
                        <div class="card " style="width: 300px; margin: 15px;">
                            <!--imagem
                            ==============================================-->
                            <img class="card-img-top" src="<?php echo "Img_Uploads/".$dado["imgcar"];?>" alt="Não Possui Imagem" style="height: 200px;width: 300px">

                            <div class="card-body">
                                <!--Nome Do Dono
                                ==================================================-->
                                <!--Modelo / Placa
                          ==================================================-->
                                <h4 class="card-title"><?php echo $dado["modelo"]; ?> - <?php echo $dado["placa"]; ?></h4>
                                <!--caracteristicas
                                ==============================================-->
                                <p class="card-text"><?php echo $dado["carac"]; ?></p>
                                <!--Barra de status
                                ============================================-->
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalExemplo1<?php echo $dado["id_carro"]; ?>" data-whatever="<?php echo $dado["id_carro"]; ?>" >Status</button>
                                <!--Barra de Editar
                                ===========================================-->
                                <button type="button" style="margin-left: 23px" class="btn btn-primary" data-toggle="modal" data-target="#modalExemplo2" data-whatever="<?php echo $dado["id_carro"]; ?>" >Editar</button>
                                <!--Barra de Exclusao
                                =======================================================================-->
                                <button type="button" style="margin-left: 23px" class="btn btn-danger" data-toggle="modal" data-target="#modalExemplo3" data-whatever="<?php echo $dado["id_carro"]; ?>" >Excluir</button>
                                <!--======================================================================-->
                            </div>
                        </div>
                    </div>
<!-------------------------------------------->
        <div class="modal fade" id="modalExemplo2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="color: black">
                    <form action="edit_carros.php" enctype="multipart/form-data" method="post">
                        <div class="form-group">
                            <input type="hidden" contenteditable="false" class="form-control" id="id" name="id">
                        </div>
                        <div class="modal-header">
                            <h5 class="modal-title" style="text-align: center;font-size: 30px;font-family: Roboto;" id="exampleModalLabe">Editar Veículo</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="mod" class="col-form-label">Modelo do Veículo <a style="color: red">*</a></label>
                                <input type="text" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control"  id="recipient-name" name="modelo" >
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Placa do Veículo <a style="color: red">*</a></label>
                                <input type="text" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" name="placa" size="8" maxlength="8" >
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Ano do Veículo <a style="color: red">*</a></label>
                                <input type="text" style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" size="4" maxlength="4" name="ano" pattern="[0-9]+$" >
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Características do Veículo <a style="color: red">*</a></label>
                                <textarea style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7" class="form-control" id="recipient-name" rows="3" name="carac" ></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label ">Imagem Referente ao Veículo</label>
                                <br>
                                <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                                <div><input name="img" type="file"/></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <input type="submit" class="btn btn-primary btn-lg" value="Editar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
<!--------------------------->
        <div class="modal fade" id="modalExemplo3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="excluir.php" method="post">
                        <div class="form-group">
                            <input type="hidden" contenteditable="false" class="form-control" id="id" name="id">
                        </div>
                        <div class="modal-header">
                            <h5 class="modal-title" style="color: black;font-size: 30px" id="exampleModalLabel">Deseja Realmente Excluir?</h5>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <input type="submit" class="btn btn-danger btn-lg" value="Excluir">
                        </div>
                    </form>
                </div>
            </div>
        </div>
<!--------------------------->
        <div class="modal fade" id="modalExemplo1<?php echo $dado["id_carro"]; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" style="color: black;font-size: 30px" id="exampleModalLabel">Status</h5>
                    </div>
                    <div class="modal-body" style="color: black">
                        <form method="post" action="edit_status.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="hidden" value="<?php echo $dado["id_carro"]; ?>" contenteditable="false" class="form-control" name="id">
                            </div>
                            <label class="text-primary" style="text-align: center;font-size: 20px;font-family: Roboto;">Status desejado :</label><br>
                            <select name="status">
                                <option name="status" value="0" >Selecione o Status</option>
                                <option class="text-danger" name="status" value="1">Solicitando Conserto</option>
                                <option class="text-warning" name="status" value="2">Em Andamento</option>
                                <option class="text-success" name="status" value="3">Pronto Para Retirada</option>
                            </select>
                            <br>
                            <br>
                            <label class="text-primary" style="text-align: center;font-size: 20px;font-family: Roboto;">Observações:</label>
                            <textarea style="background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;" class="form-control" id="recipient-name" name="obs"><?php echo utf8_encode($dado["obs"]); ?></textarea>
                            <br>
                            <label for="recipient-name" class="col-form-label text-primary">Video de Alterações</label>
                            <br>
                            <input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
                            <div><input name="video" type="file"/></div>
                            <br>
                            <label class="text-big" style="text-align: center;font-size: 20px;font-family: Roboto;">Status Atual :</label><br>
                            <label style="text-align: center;font-size: 20px;font-family: Roboto;">-----------------------------------------------------</label><br>

                            <p class="card-text"><?php
                                if ($dado["status"]==1){
                                    echo "<a class='text-danger' style='font-size: 25px'>Solicitando conserto<a/><br><br>";
                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >".utf8_encode($dado["obs"])."</textarea>";
                                }
                                if ($dado["status"]==2){
                                    echo "<a class='text-warning' style='font-size: 25px'>Em Andamento<a/><br><br>";
                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >".utf8_encode($dado["obs"])."</textarea>";
                                }
                                if ($dado["status"]==3) {
                                    echo "<a class='text-success' style='font-size: 25px'>Pronto Para Retirada!<a/><br><br>";
                                    echo "<textarea style=\"background-color: #ffffff;color: #0f0f0f;border-color: #b9c0c7;height: 100px;\" readonly=\"readonly\" class=\"form-control\" id=\"recipient-name\" >".utf8_encode($dado["obs"])."</textarea>";
                                    ?><br><br>
                                    <video width="400" controls="controls" >
                                        <source  type="video/mp4" src="<?php echo "Video_Up/".$dado["video"];?>">
                                    </video>
                                <?php } ?>
                            </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <input type="submit" class="btn btn-primary btn-lg" value="Editar">
                    </div>
                    </form>
                </div>
            </div>
        </div>
            <?php } ?>
            </div>
 <?php }
?>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
        <!-- bootstrap JS
            ============================================ -->
        <script src="js/bootstrap.min.js"></script>
        <!-- wow JS
            ============================================ -->
        <script src="js/wow.min.js"></script>
        <!-- price-slider JS
            ============================================ -->
        <script src="js/jquery-price-slider.js"></script>
        <!-- meanmenu JS
            ============================================ -->
        <script src="js/jquery.meanmenu.js"></script>
        <!-- owl.carousel JS
            ============================================ -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- sticky JS
            ============================================ -->
        <script src="js/jquery.sticky.js"></script>
        <!-- scrollUp JS
            ============================================ -->
        <script src="js/jquery.scrollUp.min.js"></script>
        <!-- mCustomScrollbar JS
            ============================================ -->
        <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
        <!-- metisMenu JS
            ============================================ -->
        <script src="js/metisMenu/metisMenu.min.js"></script>
        <script src="js/metisMenu/metisMenu-active.js"></script>
        <!-- sparkline JS
            ============================================ -->
        <script src="js/sparkline/jquery.sparkline.min.js"></script>
        <script src="js/sparkline/jquery.charts-sparkline.js"></script>
        <!-- calendar JS
            ============================================ -->
        <!-- float JS
            ============================================ -->
        <script src="js/flot/jquery.flot.js"></script>
        <script src="js/flot/jquery.flot.resize.js"></script>
        <script src="js/flot/curvedLines.js"></script>
        <script src="js/flot/flot-active.js"></script>
        <!-- plugins JS
            ============================================ -->
        <script src="js/plugins.js"></script>
        <!-- main JS
            ============================================ -->
        <script src="js/main2.js"></script>

        <script type="text/javascript">
            $('#modalExemplo2').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                var mod = button.data('whatevermod') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#id').val(recipient)
            })
            $('#modalExemplo3').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                var mod = button.data('whatevermod') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#id').val(recipient)
            })
            $('#modalExemplo1').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                var mod = button.data('whatevermod') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#id').val(recipient)
            })
        </script>
