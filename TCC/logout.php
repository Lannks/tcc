<?php
session_start();
include ('login.php');
$token = md5(session_id());
if(isset($_GET['token']) && $_GET['token'] === $token) {
    // limpe tudo que for necessário na saída.
    // Eu geralmente não destruo a seção, mas invalido os dados da mesma
    // para evitar algum "necromancer" recuperar dados. Mas simplifiquemos:
    session_destroy();
    session_unset();
    unset($nome);
    header("location: index.php");
    exit();
} else {
    echo 'erro';
}