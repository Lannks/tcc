-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23-Out-2019 às 03:07
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `securecarfix`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `carro`
--

CREATE TABLE IF NOT EXISTS `carro` (
  `id_carro` int(10) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(50) NOT NULL,
  `placa` varchar(15) NOT NULL,
  `ano` varchar(4) NOT NULL,
  `carac` varchar(275) NOT NULL,
  `imgcar` varchar(500) DEFAULT NULL,
  `status` varchar(5) NOT NULL,
  `obs` varchar(300) DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id_carro`),
  KEY `id_cliente_fk` (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `carro`
--

INSERT INTO `carro` (`id_carro`, `modelo`, `placa`, `ano`, `carac`, `imgcar`, `status`, `obs`, `id_cliente`) VALUES
(1, 'corsa', '12312312', '1231', 'asdasd', '15703054495d98f5a906541', '3', NULL, 7),
(2, 'corsa', '12312312', '1231', 'lindo', '15717797015daf74753d01c.jpg', '1', NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `img_user` varchar(400) DEFAULT NULL,
  `usuario` varchar(20) NOT NULL,
  `cpf` varchar(15) NOT NULL,
  `data` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(15) NOT NULL,
  `priv` varchar(15) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `img_user`, `usuario`, `cpf`, `data`, `email`, `senha`, `priv`) VALUES
(1, '15708117285da0af5096cca.jpg', '2', '123123', '2019-09-05', 'contatolannk@gmail.com', '1', '1'),
(7, '15703063025d98f8fead448.jpg', '1', '13123', '2019-10-02', 'contatolannk@gmail.com', '1', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `color` varchar(50) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `events`
--

INSERT INTO `events` (`id`, `title`, `color`, `start`, `end`) VALUES
(1, 'pipoca', 'blue', '2019-10-09 00:00:00', '2019-10-11 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `registros`
--

CREATE TABLE IF NOT EXISTS `registros` (
  `id_registro` int(11) NOT NULL AUTO_INCREMENT,
  `id_carro` int(11) NOT NULL,
  `status` varchar(200) NOT NULL,
  `obs` text NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id_registro`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `carro`
--
ALTER TABLE `carro`
  ADD CONSTRAINT `carro_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
