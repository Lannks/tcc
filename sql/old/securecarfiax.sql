-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11-Nov-2019 às 03:07
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `securecarfix`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `carro`
--

CREATE TABLE IF NOT EXISTS `carro` (
  `id_carro` int(10) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(50) NOT NULL,
  `placa` varchar(15) NOT NULL,
  `ano` varchar(4) NOT NULL,
  `carac` varchar(275) NOT NULL,
  `imgcar` varchar(500) DEFAULT NULL,
  `status` varchar(5) NOT NULL,
  `obs` varchar(300) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id_carro`),
  KEY `id_cliente_fk` (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=509 ;

--
-- Extraindo dados da tabela `carro`
--

INSERT INTO `carro` (`id_carro`, `modelo`, `placa`, `ano`, `carac`, `imgcar`, `status`, `obs`, `video`, `id_cliente`) VALUES
(504, 'corsa', 'asdadddd', '1231', 'loko', '15733547325dc77cec00a8f.jpg', '3', 'oi', NULL, 7),
(506, 'asdasd', '12312312', '1', 'dasd', '15734318685dc8aa3c73dbf', '2', 'pega pelo amor de god', NULL, 7),
(507, 'MANO DO CEU', 'sla', '2312', 'sdasd', '15733531725dc776d447b3e', '2', 'oi', '15733546025dc77c6a739c2', 1),
(508, 'asdasd', '12312312', '1', 'dasd', '15734318685dc8aa3c73dbf', '1', 'dasdasd', NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `img_user` varchar(400) DEFAULT NULL,
  `usuario` varchar(20) NOT NULL,
  `cpf` varchar(15) NOT NULL,
  `data` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(500) NOT NULL,
  `priv` varchar(15) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `img_user`, `usuario`, `cpf`, `data`, `email`, `senha`, `priv`) VALUES
(1, '15708117285da0af5096cca.jpg', '2', '123123', '2019-09-05', 'contatolannk@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', '1'),
(7, '15703063025d98f8fead448.jpg', '1', '13123', '2019-10-02', 'contatolannk@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', '0'),
(10, 'Foto_Padrao.jpg', 'a', '13123', '2019-11-05', 'contatolannk@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `color` varchar(50) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `registros`
--

CREATE TABLE IF NOT EXISTS `registros` (
  `id_registro` int(11) NOT NULL AUTO_INCREMENT,
  `id_carro` varchar(11) NOT NULL,
  `status` varchar(200) NOT NULL,
  `obs` text NOT NULL,
  `data` varchar(100) NOT NULL,
  PRIMARY KEY (`id_registro`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `registros`
--

INSERT INTO `registros` (`id_registro`, `id_carro`, `status`, `obs`, `data`) VALUES
(4, '506', '2', 'pega pelo amor de god', '0000-00-00'),
(5, '508', '2', 'coe', '0000-00-00'),
(6, '508', '', 'to usando hack', '0000-00-00'),
(7, '508', '1', 'aaaaaaaaaa', '2011-11-15'),
(8, '508', '', 'aaaaaaaaaah', '2011-11-19'),
(9, '508', '', 'aaaaaaaaaahf', '11/11/19'),
(10, '508', '', 'ad', '11/11/2019 02:27:29'),
(11, '508', '', 'dasdasd', '10/11/2019 23:29:02');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `carro`
--
ALTER TABLE `carro`
  ADD CONSTRAINT `carro_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
